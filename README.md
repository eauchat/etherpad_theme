# Etherpad theme customized for eauchat

This repository contains the theme used on the eauchat etherpad instance.
It is based on the colibris theme, slightly modified.

This should be cloned into `/var/www/etherpad_mypads/src/static/skins`.
To enable the new theme , open https://pad.eauchat.org/admin/settings and change the value after `skinName`. Or edit the file: `/var/www/etherpad_mypads/settings.json` and then run `sudo yunohost service restart etherpad_mypads`.

Normally, only the `index.css` and `pad.css` file are modified from the colibris theme version. So you can pull latest changes to etherpad-lite repo, rsync the whole colibris theme (etherpad-lite/src/static/skins/colibris) to this repo, restore the few changes made in `index.css` (+ re-add the corresponding background image in `images`).
